// croll menu page fixed
$(window).bind('scroll', function () {
    if ($(window).scrollTop() > 0) {
        $('.header-fixed').addClass('fixed-bgr');
    } else {
        $('.header-fixed').removeClass('fixed-bgr');
    }
});


// menu
$(document).ready(function(){
  $('.btn-toggle-menu').click(function(e){
    e.preventDefault(); 
    e.stopPropagation()
    $('.menu').toggleClass('active');
    $('.btn-toggle-menu').toggleClass('hambger');
    $('body').toggleClass('bgr-op');
  })
  $('.menu').click( function(e) {
    e.stopPropagation(); 
  }); 
    
  $('body').click( function() {
    $('.menu').removeClass('active');
    $('.btn-toggle-menu').removeClass('hambger')
    $('body').removeClass('bgr-op');
  });
});



// stop close modal
$(".modal").on('hidden.bs.modal', function (e) {
  $(".modal iframe").each(function(){
    $(this).attr("src", $(this).attr("src"));
  });
});

// slide-video
$('.owl-videos').owlCarousel({
  loop: true,
  items: 1,
  margin: 0,
  nav: true,
  dots:false,
  autoHeight: true,
  onTranslate: function(event) {

    var currentSlide, player, command;

    currentSlide = $('.owl-item.active');

    player = currentSlide.find(".flex-video iframe").get(0);

    command = {
      "event": "command",
      "func": "pauseVideo"
    };

    if (player != undefined) {
      player.contentWindow.postMessage(JSON.stringify(command), "*");

    }
  }

});
$('.owl-videos2').owlCarousel({
  loop: true,
  items: 1,
  margin: 0,
  nav: true,
  dots:false,
  autoHeight: true,
  onTranslate: function(event) {

    var currentSlide, player, command;

    currentSlide = $('.owl-item.active');

    player = currentSlide.find(".flex-video iframe").get(0);

    command = {
      "event": "command",
      "func": "pauseVideo"
    };

    if (player != undefined) {
      player.contentWindow.postMessage(JSON.stringify(command), "*");

    }
  }

});



// slide feedback
$('.content-feedback').owlCarousel({
  loop: false,
  items: 1,
  margin: 0,
  nav: true,
  dots:false,
  autoHeight: true,
  URLhashListener:true,
  autoplayHoverPause:true,
  startPosition: 'URLHash',

});

$('.link-btn-user').on('click', function(){
  $('.link-btn-user').removeClass('active');
  $(this).addClass('active');
});


// Back to top
  $(".back-to-top a").click(function (n) {
      n.preventDefault();
      $("html, body").animate({
          scrollTop: 0
      }, 500)
  });
  $(window).scroll(function () {
      $(document).scrollTop() > 1e3 ? $(".back-to-top").addClass("display") : $(".back-to-top").removeClass("display")
  });


 // scroll #a
$('a').click(function(){
    $('html, body').animate({
        scrollTop: $( $(this).attr('href') ).offset().top
    }, 500); 
    $(".menu").removeClass('active');
    $('body').removeClass('bgr-op');
    $('.btn-toggle-menu').removeClass('hambger');
    return false;
});




// show-hide-text
$(".show-hide-list").on("click", function () {
    var listlinks = $(".list-links").is(':visible') ? '[ Xem thêm ]' : '[ Ẩn ]';
    $(".show-hide-list").text(listlinks);
    $(this).parent().next('.list-links').slideToggle(500);
});



// fixed-menu
  var section = $('#fix_header');
  var start = $(section).offset().top;
  $.event.add(window, "scroll", function () {
    
    var p = $(window).scrollTop(); $(section).css('position', ((p) > start) ? 'fixed' : ''); 
    $(section).css('top', ((p) > start) ? '0px' : '');
    $(section).css('left', ((p) > start) ? '0px' : '');
    $(section).css('width', ((p) > start) ? '100%' : '');
    $(section).css('box-shadow', ((p) > start) ? '0 2px 2px rgba(0, 0, 0, 0.17)' : '');
    
    if ($(window).width() > 991) {
      if (p <= 190) {  
        $(".scrollHeader").css("left","unset");
        $(section).removeClass('fixed-bgr');
        $('.content-header').css('margin-top','40px');

      } else { 
        $(".scrollHeader").css("left","0");
        $(section).addClass('fixed-bgr');
        $('.content-header').css('margin-top','90px');
      }
    }
    else{
      if (p <= 0) { 
       $(section).removeClass('fixed-bgr'); 
       $('.content-header').css('margin-top','100px')
       
      } else { 
        $(section).addClass('fixed-bgr');  
        $('.content-header').css('margin-top','170px')
      }
    }
  });
